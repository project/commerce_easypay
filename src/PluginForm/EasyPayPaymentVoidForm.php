<?php

/**
 * Provides Commerce integration for EasyPay payment gateway.
 *
 * PHP VERSION 7
 *
 * @category  Commerce
 * @package   Commerce_EasyPay
 * @author    Filipe Certal <filipe.certal@gmail.com>
 * @copyright 2020 Webouse * https://www.drupal.org/webouse
 * @license   http://www.gnu.org/licenses/gpl-3.0.txt GPLv3
 * @link      https://www.drupal.org/project/commerce_easypay Project page
 */

namespace Drupal\commerce_easypay\PluginForm;

use Drupal\commerce_payment\PluginForm\PaymentGatewayFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * EasyPay (Drupal Commerce Payment Gateway) class
 *
 * @category Commerce
 * @package  Commerce_EasyPay
 * @author   Filipe Certal <filipe.certal@gmail.com>
 * @license  http://www.gnu.org/licenses/gpl-3.0.txt GPLv3
 * @link     https://www.drupal.org/project/commerce_easypay Project page
 */
class EasyPayPaymentVoidForm extends PaymentGatewayFormBase
{


    /**
     * {@inheritdoc}
     */
    public function buildConfigurationForm(
        array $form,
        FormStateInterface $form_state
    ) {

        /**
         * Object Payment
         *
         * @var \Drupal\commerce_payment\Entity\PaymentInterface $payment
         * */
        $payment = $this->entity;
        $order = $payment->getOrder();
        if (!$order) {
            throw new \InvalidArgumentException(
                'Payment entity with no order reference given to EasyPayPaymentVoidForm.'
            );
        }

        $form['label'] = array(
            '#type' => 'html_tag',
            '#tag' => 'p',
            '#value' => $this->t('Do you want to void the payment of ') .
                $payment->getAmount() . '?',
        );

        return $form;
    }

    /**
     * {@inheritdoc}
     */
    public function submitConfigurationForm(
        array &$form,
        FormStateInterface $form_state
    ) {

        /**
         * Payment Object
         *
         * @var \Drupal\commerce_payment\Entity\PaymentInterface $payment
         * */
        $payment = $this->entity;

        /**
         * Manual Payment Gateway Object
         *
         * @var \Drupal\commerce_payment\Plugin\Commerce\PaymentGateway\ManualPaymentGatewayInterface
         *  $payment_gateway_plugin
         * */
        $payment_gateway_plugin = $this->plugin;
        $payment_gateway_plugin->voidPayment($payment);
    }
}
