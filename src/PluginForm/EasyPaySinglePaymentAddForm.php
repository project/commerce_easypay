<?php

/**
 * Provides Commerce integration for EasyPay payment gateway.
 *
 * PHP VERSION 7
 *
 * @category  Commerce
 * @package   Commerce_EasyPay
 * @author    Filipe Certal <filipe.certal@gmail.com>
 * @copyright 2020 Webouse * https://www.drupal.org/webouse
 * @license   http://www.gnu.org/licenses/gpl-3.0.txt GPLv3
 * @link      https://www.drupal.org/project/commerce_easypay Project page
 */

namespace Drupal\commerce_easypay\PluginForm;

use Drupal\commerce_payment\PluginForm\PaymentGatewayFormBase;
use Drupal\Core\Form\FormStateInterface;
use \Drupal\commerce_price\Price;

/**
 * EasyPay (Drupal Commerce Payment Gateway) class
 *
 * @category Commerce
 * @package  Commerce_EasyPay
 * @author   Filipe Certal <filipe.certal@gmail.com>
 * @license  http://www.gnu.org/licenses/gpl-3.0.txt GPLv3
 * @link     https://www.drupal.org/project/commerce_easypay Project page
 */
class EasyPaySinglePaymentAddForm extends PaymentGatewayFormBase
{

    /**
     * {@inheritdoc}
     */
    public function buildConfigurationForm(
        array $form,
        FormStateInterface $form_state
    ) {
        /**
         * Payment Object
         *
         * @var \Drupal\commerce_payment\Entity\PaymentInterface $payment
         * */
        $payment = $this->entity;
        $order = $payment->getOrder();
        if (!$order) {
            throw new \InvalidArgumentException(
                'Payment entity with no order reference given to PaymentAddForm.'
            );
        }

        $form['amount'] = [
            '#type' => 'commerce_price',
            '#title' => t('Amount'),
            '#default_value' => $order->getBalance()->toArray(),
            '#required' => true,
        ];
        $form['received'] = [
            '#type' => 'checkbox',
            '#title' => t('The specified amount was already received.'),
        ];

        return $form;
    }

    /**
     * {@inheritdoc}
     */
    public function submitConfigurationForm(
        array &$form,
        FormStateInterface $form_state
    ) {

        $values = $form_state->getValue($form['#parents']);
        /**
         * Payment Object
         *
         * @var \Drupal\commerce_payment\Entity\PaymentInterface $payment
         * */
        $payment = $this->entity;

        $payment->setAmount(
            new Price(
                $values['amount']['number'],
                $values['amount']['currency_code'],
            )
        );

        /**
         * Manual Payment Gateway Object
         *
         * @var \Drupal\commerce_payment\Plugin\Commerce\PaymentGateway\ManualPaymentGatewayInterface
         *  $payment_gateway_plugin
         * */
        $payment_gateway_plugin = $this->plugin;
        $payment_gateway_plugin->createPayment($payment, $values['received']);
    }
}
