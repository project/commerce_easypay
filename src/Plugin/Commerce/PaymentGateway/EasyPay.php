<?php

/**
 * Provides Commerce integration for EasyPay payment gateway.
 *
 * PHP VERSION 7
 *
 * @category  Commerce
 * @package   Commerce_EasyPay
 * @author    Filipe Certal <filipe.certal@gmail.com>
 * @copyright 2020 Webouse * https://www.drupal.org/webouse
 * @license   http://www.gnu.org/licenses/gpl-3.0.txt GPLv3
 * @link      https://www.drupal.org/project/commerce_easypay Project page
 */

namespace Drupal\commerce_easypay\Plugin\Commerce\PaymentGateway;

use Drupal\commerce_payment\Plugin\Commerce\PaymentGateway\PaymentGatewayBase;
use Drupal\commerce_payment\Plugin\Commerce\PaymentGateway\ManualPaymentGatewayInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\commerce_payment\Entity\PaymentInterface;
use Drupal\commerce_payment\Entity\PaymentMethodInterface;
use Drupal\commerce_price\Price;
use \Drupal\commerce_payment\Exception\PaymentGatewayException;
use \Drupal\Core\Entity\EntityStorageException;
use \Drupal\commerce_payment\Exception\InvalidRequestException;
use InvalidArgumentException;
use DateInterval;

/**
 * EasyPay (Drupal Commerce Payment Gateway) class
 *
 * @CommercePaymentGateway(
 *  id = "easypay",
 *  label = "EasyPay",
 *  display_label = "EasyPay",
 *  forms = {
 *    "add-payment" =
 *      "Drupal\commerce_easypay\PluginForm\EasyPaySinglePaymentAddForm",
 *    "receive-payment" =
 *      "Drupal\commerce_easypay\PluginForm\EasyPayPaymentReceiveForm",
 *    "refund_payment" =
 *      "Drupal\commerce_easypay\PluginForm\EasyPayPaymentRefundForm",
 *    "void_payment" =
 *      "Drupal\commerce_easypay\PluginForm\EasyPayPaymentVoidForm",
 *  },
 *  modes = {"Sandbox", "Production"},
 *  payment_type = "payment_default",
 *  payment_method_types = {"credit_card"},
 * )
 *
 * @category  Commerce
 * @package   Commerce_EasyPay
 * @author    Filipe Certal <filipe.certal@gmail.com>
 * @copyright 2020 Webouse * https://www.drupal.org/webouse
 * @license   http://www.gnu.org/licenses/gpl-3.0.txt GPLv3
 * @link      https://www.drupal.org/project/commerce_easypay
 */
class EasyPay extends PaymentGatewayBase implements ManualPaymentGatewayInterface
{
    /**
     * {@inheritdoc}
     * */
    public function buildConfigurationForm(
        array $form,
        FormStateInterface $form_state
    ) {
        $form = parent::buildConfigurationForm($form, $form_state);

        $form['account_id'] = array(
            '#type' => 'textfield',
            '#title' => $this->t('Account ID'),
            '#description' => $this->t('This is the Account ID from EasyPay.'),
            '#default_value' => \Drupal::state()->get('account_id') ?? '',
            '#required' => true,
        );

        $form['api_key'] = array(
            '#type' => 'textfield',
            '#title' => $this->t('API Key'),
            '#description' => $this->t('This is the API Key from EasyPay.'),
            '#default_value' => \Drupal::state()->get('api_key') ?? '',
            '#required' => true,
        );

        $form['message'] = array(
            '#type' => 'textfield',
            '#title' => $this->t("Payment message"),
            '#description' => $this->t('This is the payment introductory message.'),
            '#default_value' => \Drupal::state()->get('message') ?? '',
            '#required' => true,
        );

        return $form;
    }

    /**
     * {@inheritDoc}
     */
    public function submitConfigurationForm(
        array &$form,
        FormStateInterface $form_state
    ) {
        parent::submitConfigurationForm($form, $form_state);

        $values = $form_state->getValues($form['#parents']);

        $configuration = $values['configuration']['easypay'];
        \Drupal::state()->set('account_id', $configuration['account_id']);
        \Drupal::state()->set('api_key', $configuration['api_key']);
        \Drupal::state()->set('message', $configuration['message']);
    }

    /**
     * {@inheritDoc}
     */
    public function buildPaymentOperations(PaymentInterface $payment)
    {
        $payment_state = $payment->getState()->value;

        $operations = array(
            'receive' => array(
                'title' => $this->t('Receive'),
                'page_title' => $this->t('Receive payment'),
                'plugin_form' => 'receive-payment',
                'access' => in_array($payment_state, ['pending', 'authorization']),
            ),
            'void' => array(
                'title' => $this->t('Void'),
                'page_title' => $this->t('Void payment'),
                'plugin_form' => 'void_payment',
                'access' => in_array(
                    $payment_state,
                    ['new', 'pending', 'authorization']
                ),
            ),
            'refund' => array(
                'title' => 'Refund',
                'page_title' => 'Refund payment',
                'plugin_form' => 'refund_payment',
                'access' => $payment_state == 'completed',
            ),
        );

        return $operations;
    }

    /**
     * {@inheritDoc}
     */
    public function createPayment(PaymentInterface $payment, $received = false)
    {
        $this->assertPaymentState($payment, ['new']);

        $payment->state = $received ? 'completed' : 'pending';
        $payment->save();
    }

    /**
     * {@inheritDoc}
     */
    public function receivePayment(PaymentInterface $payment, ?Price $amount = null)
    {

        try {
            $payment->setState('completed')->save();
        } catch (EntityStorageException $exception) {

            \Drupal::logger('commerce_easypay')->error($exception->getMessage());
        }
    }

    /**
     * {@inheritDoc}
     */
    public function voidPayment(PaymentInterface $payment)
    {
        try {
            $payment->delete();
        } catch (EntityStorageException $exception) {
            throw new PaymentGatewayException($exception->getMessage());
        }
    }

    /**
     * {@inheritDoc}
     */
    public function refundPayment(PaymentInterface $payment, ?Price $amount = null)
    {
        try {
            // Verify the payment state (i.e., completed or partially refunded).
            $this->assertPaymentState($payment, ['completed', 'partially_refunded']);

            // If not specified, refund the entire amount.
            $amount = $amount ?: $payment->getAmount();
            $this->assertRefundAmount($payment, $amount);

            // Determine whether payment has been fully or partially refunded.
            $old_refunded_amount = $payment->getRefundedAmount();
            $new_refunded_amount = $old_refunded_amount->add($amount);
            if ($new_refunded_amount->lessThan($payment->getAmount())) {
                $payment->setState('partially_refunded');
            } else {
                $payment->setState('refunded');
            }

            $payment->setRefundedAmount($new_refunded_amount);
            $payment->save();
        } catch (InvalidArgumentException | InvalidRequestException |
        EntityStorageException $exception) {

            throw new PaymentGatewayException($exception->getMessage());
        }
    }

    /**
     * {@inheritDoc}
     */
    public function buildPaymentInstructions(PaymentInterface $payment)
    {

        // Prepare the variables for an error state
        $entity = null;
        $reference = null;
        $amount = null;
        $currency = null;
        $message = $this->t(
            'An error ocurred while issuing the payment references,
                    please contact the site\'s administrator'
        );

        /**
         * Order Object
         *
         * @var Drupal\commerce_order\Entity\Order $order
         */
        $order = $payment->getOrder();

        // EasyPay Authentication Keys and payment message
        $account_id = \Drupal::state()->get('account_id');
        $api_key = \Drupal::state()->get('api_key');

        if ($payment->get('remote_id')->value === null) {
            // Create a new payment.

            // Get some order fields
            $email = $order->getEmail();
            $order_number = sprintf("%08d", $order->getOrderNumber());
            $amount = sprintf("%.2f", $order->getTotalPrice()->getNumber());
            $currency = $order->getTotalPrice()->getCurrencyCode();
            /**
             * Profile Object
             *
             * @var Drupal\profile\Entity\Profile $profile
             */
            $profile = $order->getBillingProfile();

            $phone = $profile->field_telephone->value;
            $fiscal_number = $profile->field_nif->value;

            /**
             * Customer Object
             *
             * @var Drupal\user\Entity\User $customer
             */
            $customer = $order->getCustomer();

            $name = $profile->address->first();
            $fullname =  $name->given_name . ' ' . $name->family_name;

            // EasyPay Authentication Keys and payment message
            $account_id = \Drupal::state()->get('account_id');
            $api_key = \Drupal::state()->get('api_key');
            $message = \Drupal::state()->get('message');

            $expiration_time = new \Datetime();
            $expiration_time = $expiration_time
                ->add(new DateInterval("P2D"))
                ->format('Y-m-d H:i');

            // Prepare API call
            $curl = curl_init();

            curl_setopt_array(
                $curl,
                array(
                    CURLOPT_URL => "https://api.test.easypay.pt/2.0/single",
                    CURLOPT_RETURNTRANSFER => true,
                    CURLOPT_ENCODING => "",
                    CURLOPT_MAXREDIRS => 10,
                    CURLOPT_TIMEOUT => 0,
                    CURLOPT_FOLLOWLOCATION => true,
                    CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                    CURLOPT_CUSTOMREQUEST => "POST",
                    CURLOPT_POSTFIELDS =>
                    "{
                    \"expiration_time\": \"$expiration_time\",
                    \"customer\": {
                        \"name\": \"$fullname\",
                        \"email\": \"$email\",
                        \"phone\": \"$phone\",
                        \"phone_indicative\": \"+351\",
                        \"fiscal_number\": \"$fiscal_number\",
                        \"key\": \"ORDER$order_number\",
                        \"language\": \"PT\"
                    },\n
                    \"key\": \"ORDER$order_number\",
                    \"value\": $amount,
                    \"method\": \"mb\"
                }",
                    CURLOPT_HTTPHEADER => array(
                        ": ",
                        "AccountId: $account_id",
                        "ApiKey: $api_key",
                        "Content-Type: application/json"
                    ),
                )
            );

            $response = json_decode(curl_exec($curl));

            curl_close($curl);

            if ($response->status === 'ok') {

                // Get information from the API answer
                $entity = $response->method->entity;
                $reference = $response->method->reference;
                $remote_id = $response->id;
                $message = \Drupal::state()->get('message');

                //Set the Payment's Remote ID
                $payment->setRemoteId($remote_id);

                //Set the Payment state to 'pending'
                $payment->setState('authorization');
                $payment->setRemoteState($response->method->status);
                $payment->save();
            }
        } else {
            // Retrieve data from previous issued payment.

            $remote_id = $payment->get('remote_id')->value;

            // Prepare de API call
            $curl = curl_init();

            curl_setopt_array(
                $curl,
                array(
                    CURLOPT_URL => "https://api.test.easypay.pt/2.0/single/" .
                        $remote_id,
                    CURLOPT_RETURNTRANSFER => true,
                    CURLOPT_ENCODING => "",
                    CURLOPT_MAXREDIRS => 10,
                    CURLOPT_TIMEOUT => 0,
                    CURLOPT_FOLLOWLOCATION => true,
                    CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                    CURLOPT_CUSTOMREQUEST => "GET",
                    CURLOPT_HTTPHEADER => array(
                        "AccountId: $account_id",
                        "ApiKey: $api_key"
                    ),
                )
            );

            $response = json_decode(curl_exec($curl));

            curl_close($curl);

            if (!(isset($responde['status']) && $response->status === 'error')) {

                // Get data from response
                $entity = $response->method->entity;
                $reference = $response->method->reference;
                $amount = $response->value;
                $currency = $response->currency;
                $message = \Drupal::state()->get('message');
            }
        }

        return array(
            '#theme' => 'mb_references',
            '#entity' => $entity,
            '#reference' => $reference,
            '#amount' => $amount,
            '#currency' => $currency,
            '#message' => $message,
        );
    }
}
